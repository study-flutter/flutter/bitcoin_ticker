import './networking.dart';

// const apikey = 'E31DBE92-A509-4847-A727-76DCBC57AD81';
const apikey = '8202B178-AFDE-4BCC-9E9F-613899A8E384';
const url = 'https://rest.coinapi.io/v1/exchangerate';

class CoinModel {
  Future<dynamic> getCoinPrice(String coinName, String currency) async {
    NetworkHelper networkHelper =
        NetworkHelper('$url/$coinName/$currency?apikey=$apikey');

    var decodedData = await networkHelper.getData();
    return decodedData['rate'];
  }
}
