import 'package:bitcoin_ticker/services/coin.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Item extends StatefulWidget {
  Item({@required this.currency, @required this.coinName});
  final currency;
  final coinName;
  @override
  _ItemState createState() => _ItemState();
}

class _ItemState extends State<Item> {
  String value = 'loading';
  CoinModel coinInfo = CoinModel();
  Future<String> getValue() async {
    double price =
        await coinInfo.getCoinPrice(widget.coinName, widget.currency);
    setState(() {
      value = price.toStringAsFixed(0);
    });
  }

  Widget build(BuildContext context) {
    getValue();
    return Padding(
      padding: EdgeInsets.fromLTRB(18.0, 18.0, 18.0, 0),
      child: Card(
        color: Colors.lightBlueAccent,
        elevation: 5.0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: 15.0, horizontal: 28.0),
          child: Text(
            '1 ${widget.coinName} = ${value} ${widget.currency}',
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 20.0,
              color: Colors.white,
            ),
          ),
        ),
      ),
    );
  }
}
